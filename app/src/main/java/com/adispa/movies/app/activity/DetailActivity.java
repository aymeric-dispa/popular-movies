package com.adispa.movies.app.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adispa.movies.R;
import com.squareup.picasso.Picasso;

import java.util.Map;

import static com.adispa.movies.app.Constants.*;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.activity_detail, new DetailFragment())
                    .commit();
        }
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class DetailFragment extends Fragment {

        private static final String LOG_TAG = DetailFragment.class.getSimpleName();


        public DetailFragment() {
            setHasOptionsMenu(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.content_detail, container, false);

            TextView title = (TextView) rootView.findViewById(R.id.title_textview);
            TextView releaseDate = (TextView) rootView.findViewById(R.id.release_date_textview);
            TextView synopsis = (TextView) rootView.findViewById(R.id.synopsis_textview);
            TextView rating = (TextView) rootView.findViewById(R.id.rating_textview);

            Intent intent = this.getActivity().getIntent();

            Map<String, Object> aMovie = (Map<String, Object>) intent.getSerializableExtra(Intent.EXTRA_TEXT);
            Log.i(LOG_TAG, "Loading the movie details.");
            ImageView iconView = (ImageView) rootView.findViewById(R.id.movie_icon);
            Picasso.with(this.getContext()).load("http://image.tmdb.org/t/p/" + "w185/" + aMovie.get(MDB_POSTER)).into(iconView);
            title.setText((String) aMovie.get(MDB_TITLE));
            releaseDate.setText(aMovie.get(MDB_RELEASE_DATE).toString());
            synopsis.setText((String) aMovie.get(MDB_SYNOPSIS));
            rating.setText((String) aMovie.get(MDB_RATING));
            Log.i(LOG_TAG, "Details of the movie loaded.");
            return rootView;
        }
    }


}
