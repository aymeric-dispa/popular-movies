package com.adispa.movies.app.view;

import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.widget.GridView;

/**
 * Created by Aymeric on 09-10-16.
 */

public class MoviesGridView extends GridView {


    public MoviesGridView(Context context) {
        super(context);
        updateColumnsNumber();
    }

    public MoviesGridView(Context context, AttributeSet attrs) {
        super(context, attrs);
        updateColumnsNumber();
    }

    public MoviesGridView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        updateColumnsNumber();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        updateColumnsNumber();
    }

    /**
     * Change the number of columns depending on the orientation of the device.
     */
    private void updateColumnsNumber() {
        this.setNumColumns(getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE ? 3 : 2);
    }

}
