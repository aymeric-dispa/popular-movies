package com.adispa.movies.app;

/**
 * Created by Aymeric on 10-10-16.
 */

public class Constants {
    public static final String MDB_RESULTS = "results";
    public static final String MDB_POSTER = "poster_path";
    public static final String MDB_TITLE = "title";
    public static final String MDB_SYNOPSIS = "overview";
    public static final String MDB_RATING = "vote_average";
    public static final String MDB_RELEASE_DATE = "release_date";
}
