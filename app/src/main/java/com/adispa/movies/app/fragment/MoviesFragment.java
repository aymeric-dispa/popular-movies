package com.adispa.movies.app.fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;

import com.adispa.movies.R;
import com.adispa.movies.app.activity.DetailActivity;
import com.adispa.movies.app.adapter.PicassoAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.adispa.movies.app.Constants.*;

/**
 * Created by Aymeric on 08-10-16.
 */

public class MoviesFragment extends Fragment {
    private final String LOG_TAG = MoviesFragment.class.getSimpleName();
    private ArrayAdapter<Map<String, Object>> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_main, container, false);


        // Get a reference to the ListView, and attach this adapter to it.
        GridView gridView = (GridView) rootView.findViewById(R.id.grid_view_movies);
        adapter = new PicassoAdapter(
                getActivity(), // The current context (this activity)
                R.layout.grid_item_thumbnail, // The name of the layout ID.
                R.id.grid_item_movie_imageview, // The ID of the imageview to populate.
                new ArrayList<Map<String, Object>>());
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent detailIntent = new Intent(parent.getContext(), DetailActivity.class).putExtra(Intent.EXTRA_TEXT, (HashMap)adapter.getItem(position));
                startActivity(detailIntent);
            }

        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        updateMovies();
    }


    private void updateMovies() {
        FetchMoviesTask fetchMoviesTask = new FetchMoviesTask();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        String sortOrder = prefs.getString(getString(R.string.sort_order_key), getString(R.string.sort_order_entry_default));
        fetchMoviesTask.execute(sortOrder);
    }


    private class FetchMoviesTask extends AsyncTask<String, Void, List<Map<String, Object>>> {


        protected List<Map<String, Object>> doInBackground(String... params) {
            // These two need to be declared outside the try/catch
            // so that they can be closed in the finally block.
            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;

            // Will contain the raw JSON response as a string.
            String moviesJsonStr = null;
            StringBuffer buffer = new StringBuffer();

            try {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("http")
                        .authority("api.themoviedb.org")
                        .appendPath("3")
                        .appendPath("movie")
                        .appendPath(params[0])
                        .appendQueryParameter("api_key", "TO-REPLACE");

                String urlString = builder.build().toString();
                URL url = new URL(urlString);


                // Create the request to themoviedb, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();

                if (inputStream == null) {
                    // Nothing to do.
                    moviesJsonStr = null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = reader.readLine()) != null) {
                    // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                    // But it does make debugging a *lot* easier if you print out the completed
                    // buffer for debugging.
                    buffer.append(line + "\n");
                }

                if (buffer.length() == 0) {
                    // Stream was empty.  No point in parsing.
                    moviesJsonStr = null;
                }
                moviesJsonStr = buffer.toString();
                return getMoviesDataFromJson(moviesJsonStr, 100);
            } catch (IOException | JSONException e) {
                Log.e(LOG_TAG, "Error ", e);
                // If the code didn't successfully get the movie data, there's no point in attempting
                // to parse it.
                moviesJsonStr = null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }

            }
            return null;

        }

        @Override
        protected void onPostExecute(List<Map<String, Object>> result) {
            if (result != null) {
                adapter.clear();
                for (Map<String, Object> movie : result) {
                    adapter.add(movie);
                }
                adapter.notifyDataSetChanged();
            }

        }
    }


    private List<Map<String, Object>> getMoviesDataFromJson(String movieJsonStr, int numMovies)
            throws JSONException {


        JSONObject movieJson = new JSONObject(movieJsonStr);
        JSONArray movieArray = movieJson.getJSONArray(MDB_RESULTS);

        List<Map<String, Object>> resultMovies = new ArrayList<>();
        for (int i = 0; i < movieArray.length() && i < numMovies; i++) {
            HashMap<String, Object> aMovie = new HashMap<>();
            JSONObject movie = movieArray.getJSONObject(i);

            aMovie.put(MDB_POSTER, movie.getString(MDB_POSTER));
            aMovie.put(MDB_TITLE, movie.getString(MDB_TITLE));
            aMovie.put(MDB_SYNOPSIS, movie.getString(MDB_SYNOPSIS));
            aMovie.put(MDB_RATING, movie.getString(MDB_RATING));
            aMovie.put(MDB_RELEASE_DATE, movie.getString(MDB_RELEASE_DATE));
            resultMovies.add(aMovie);

        }
        return resultMovies;

    }


}
