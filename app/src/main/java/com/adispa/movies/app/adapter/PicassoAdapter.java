package com.adispa.movies.app.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.adispa.movies.R;
import com.adispa.movies.app.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Map;

/**
 * Created by Aymeric on 09-10-16.
 */

public class PicassoAdapter extends ArrayAdapter<Map<String,Object>> {
    private final String LOG_TAG = PicassoAdapter.class.getSimpleName();

    public PicassoAdapter(Context context, int resource, int textViewResourceId, List<Map<String,Object>> objects) {
        super(context, resource, textViewResourceId, objects);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String imageUrl = (String)this.getItem(position).get(Constants.MDB_POSTER);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.grid_item_thumbnail, parent, false);
        }
        ImageView iconView = (ImageView) convertView.findViewById(R.id.grid_item_movie_imageview);
        //Loads album art thumbnail into the ImageView
        Picasso.with(parent.getContext()).load("http://image.tmdb.org/t/p/" + "w500/" + imageUrl).into(iconView);
        return iconView;
    }
}
